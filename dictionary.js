#!/usr/bin/env node

const program = require('commander');

const { getWordDefinitions, getSynonyms, getAntonyms, showUsage, showDictionary, showDictionaryForRandomWord, playGame } = require('./actions');


program
  .version('0.0.1')
  .description('Contact management system');
  // program
  // .command('[word]','Word Example').alias('w').action((word) => {return showDictionary(word,true);})
  // .command('defn <word>','Definition of a word').alias('d').action((word) => {return getWordDefinitions(word,true);})
  // .command('syn <word>','Synonym of a word').alias('s').action((word) => {return getSynonyms(word,true)})
  // .command('ant <word>','Antonym of word').alias('a').action((word) => {return getAntonyms(word,true);})
  // .command('ex <word>','Word usage examples').alias('e').action((word) => {return showUsage(word,true);})
  // .command('play','Play').alias('p').action(() => {play();})
  // .parse(process.argv)
  

program
  .command('defn <word>')
  .alias('d')
  .description('Definition of a word')
  .action((word) => {
    return getWordDefinitions(word,true);
  });

program
  .command('syn <word>')
  .alias('s')
  .description('Synonym of a word')
  .action((word) => {
    return getSynonyms(word,true);
});

program
  .command('ant <word>')
  .alias('a')
  .description('Antonym of word')
  .action((word) => {
    return getAntonyms(word,true);
});

program
  .command('ex <word>')  //random word
  .alias('e')
  .description('Word usage examples')
  .action((word) => {
    return showUsage(word,true);
});


program
  .command('play') //random word and ask user to guess
  .alias('y')
  .description('Play for it')
  .action((word) => {
    console.log('playing')
    playGame();
});

// program
//   .command('[word]') //random word and ask user to guess
//   .alias('w')
//   .description('Get contact')
//   .action((word) => {
//     console.log('word')
//     return showDictionary(word,true);
//   });

// program
//   .command('*')
//   .action(function(env){
//     console.log('env....................',process.argv)
//   })

// program
//   .command('*') //random word and ask user to guess
//   .action((env)=>{
//     console.log('env')
//     // console.log(process.argv[1])
//     // console.log(process.argv.length,'kk')
//     if(process.argv.length == 2){
//       showDictionaryForRandomWord()
//     }    
//     else if(process.argv.length === 3){
//       console.log('dic')
//       showDictionary(process.argv[2],true);
//     }
// });

if(process.argv.length == 2){
  // console.log('l')
  return showDictionaryForRandomWord()
}    
else if(process.argv.length === 3){
  // console.log('dic',process.argv[2],process.argv[2] !== 'play' )
  if(process.argv[2] === 'play'){
    // playGame();
  }
  else{
    showDictionary(process.argv[2],true);
  }
}


program.parse(process.argv);

// console.log('akhil')