let request = require('request');

const base_url = "https://fourtytwowords.herokuapp.com";
const api_key = "b972c7ca44dda72a5b482052b1f5e13470e01477f3fb97c85d5313b3c112627073481104fec2fb1a0cc9d84c2212474c0cbe7d8e59d7b95c7cb32a1133f778abd1857bf934ba06647fda4f59e878d164";

const randomWord =(callback)=>{
    let url = base_url+"/words/randomWord";
    let options = {
        method: 'GET',
        url : url,
        qs: {api_key:api_key}
    }
    request(options,function(error,response,body){
        // console.log(error);
        // console.log(response.statusCode);
        // console.log(body);
        if(!error && response.statusCode == 200){
            callback({Status:true,words:body})
        }
        else{
            callback({Status:false})
        }
    })

}

const definitions = (word,callback)=>{
    let url = base_url+'/word/'+word+'/definitions';
    let options = {
        method: 'GET',
        url : url,
        qs: {api_key:api_key}
    }
    request(options,function(error,response,body){
        if(!error && response.statusCode == 200){
            callback({Status:true,words:body}) // will be array
        }
        else{
            callback({Status:false})
        }
    })    
}

const examples = (word,callback)=>{
    let url = base_url+'/word/'+word+'/examples';
    let options = {
        method: 'GET',
        url : url,
        qs: {api_key:api_key}
    }
    request(options,function(error,response,body){
        if(!error && response.statusCode == 200){
            callback({Status:true,words:body}) // will be array
        }
        else{
            callback({Status:false})
        }
    })        
}

const relatedWords = (word,callback)=>{
    let url = base_url+'/word/'+word+'/relatedWords';
    let options = {
        method: 'GET',
        url : url,
        qs: {api_key:api_key}
    }
    request(options,function(error,response,body){
        if(!error && response.statusCode == 200){
            callback({Status:true,words:body}) // will be array
        }
        else{
            callback({Status:false})
        }
    })        
}

//Lets export them all here
module.exports = {     randomWord, definitions, relatedWords, examples };
