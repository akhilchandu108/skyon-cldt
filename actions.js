var Choices = require('prompt-choices');
const chalk = require('chalk');
var choices = new Choices(['Try Again', 'Hint', 'Quit']);
const CLI = require('clui');
const Spinner = CLI.Spinner;
const inquirer = require('inquirer');

const eventHelper  = require('./helper');

const promptForChoices = (word)=>{

    inquirer
    .prompt([
      {
        type: 'list',
        name: 'choice',
        message: 'Choose from below choices!!',
        choices: ['1. Try Again', '2. Hint', '3. Quit'],
      },
    ])
    .then(answers => {
    //   console.info('Answer:', answers.choice);
      switch(answers.choice){
        case '1. Try Again':
            console.log(chalk.green('Awesome!!..'));
            inquirer.prompt([
                {
                    name    :'word',
                    message : "Okay."
                },
            ]).then(answers=>{
                // console.log('answer',answers.word)
            })
            break;
        case '2. Hint':
            console.log(chalk.yellow('Let get some hint for you'));
            eventHelper.randomWord((data)=>{
                if(data.Status){
                    let words = data.words;
                    words = JSON.parse(words);
                    let word = words.word;
                    console.log("Let's check details for "+word);
                    getWordDefinitions(word,false);
                    getSynonyms(word,false);
                    getAntonyms(word,false);
                }
                else{
                    console.log('No words available right now.')
                }
            })
            break;
        case '3. Quit':
            console.log(chalk.magentaBright(word));
            // return;
            break;
        default:
            console.log(chalk.magentaBright('Exiting'));
            return;
      }
    });    
    
}


const getWordDefinitions = (word,promptRequired)=>{
    let status = new Spinner('Fetching word definition ...');
    status.start();
    eventHelper.definitions(word,(data)=>{
        // status.update('Almost done..!!');
        if(data.Status){
            status.stop();
            let words = data.words;
            words = JSON.parse(words);
            console.log(chalk.underline.blueBright(word+": definition:"));
            for(let i=0;i<words.length;i++){
                let text = words[i].text;
                console.log(chalk.yellow((i+1)+". "),chalk.green(text));
            }
        }
        else{
            status.stop();
            console.log(chalk.red('Np word definition found for '+word));
            if(promptRequired){
                promptForChoices(word)
            }
            return;
        }
    })
};

const getSynonyms = (word,promptRequired) =>{
    let status = new Spinner('Fetching Synonyms ...');
    status.start();
    eventHelper.relatedWords(word,(data)=>{
        // status.update('Almost done..!!');
        if(data.Status){
            status.stop();
            let words = data.words;
            words = JSON.parse(words);
            let synonyms_arr = words.filter((el)=>{return el.relationshipType === 'synonym'});
            if(synonyms_arr.length){
                // console.log(synonyms_arr);
                let synonym_words = synonyms_arr[0].words;
                console.log(chalk.underline.whiteBright("Synonyms for "+word))
                for(let i=0;i<synonym_words.length;i++){
                    console.log(chalk.white((i+1)+"."), chalk.green(synonym_words[i]));
                }
                // console.log('synonyms error');
            }
            else{
                console.log(chalk.red('No synonym found for '+word));
            }

        }
        else{
            status.stop();
            console.log(chalk.red('No synonym found for '+word));
            if(promptRequired){
                promptForChoices(word)
            }            
            return;
        }
    })
};

const getAntonyms = (word,promptRequired) => {
    let status = new Spinner('Fetching Antonyms ...');
    status.start();
    eventHelper.relatedWords(word,(data)=>{
        // status.update('Almost done..!!');
        if(data.Status){
            status.stop();
            let words = data.words;
            words = JSON.parse(words);
            let antonyms_arr = words.filter((el)=>{return el.relationshipType === 'antonym'});
            if(antonyms_arr.length){
                // console.log(antonyms_arr);
                let antonym_words = antonyms_arr[0].words;
                console.log(chalk.underline.whiteBright("Antonyms for "+word));
                for(let i=0;i<antonym_words.length;i++){
                    console.log(chalk.white((i+1)+"."),chalk.green(antonym_words[i]));
                }
                // console.log('antonyms');
            } 
            else{
                console.log(chalk.red('No antonym found for '+word));
            }
        }
        else{
            status.stop();
            console.log(chalk.red('No antonym found for '+word));
            if(promptRequired){
                promptForChoices(word)
            }            
            return;
        }
    })
}

const showUsage = (word,promptRequired) => {
    let status = new Spinner('Fetching Examples ...');
    status.start();
    eventHelper.examples(word,(data)=>{
        // status.update('Almost done..!!');
        if(data.Status){
            status.stop();
            let words = data.words;
            words = JSON.parse(words);
            
            console.log(chalk.whiteBright("Below are examples for "+word));
            for(let i=0;i<words.examples.length;i++){
                let text = words.examples[i].text;
                console.log(chalk.white((i+1)+". "),chalk.green(text));
            }
        }
        else{
            status.stop();
            console.log(chalk.red('No examples found for '+word));
            if(promptRequired){
                promptForChoices(word)
            }            
            return;
        }
    })
};

const showDictionary = (word)=>{
    let status = new Spinner('Fetching '+word+' dictionary for you...');
    status.start();
    status.stop();
    // console.log('i am in')
    getWordDefinitions(word);
    getSynonyms(word,false);
    getAntonyms(word,false);
    showUsage(word,false);
}

const showDictionaryForRandomWord = ()=>{
    let status = new Spinner('Fetching a random word');
    status.start();
    eventHelper.randomWord((data)=>{
        status.stop();
        if(data.Status){
            let words = data.words;
            words = JSON.parse(words);
            let word = words.word;
            getWordDefinitions(word);
            getSynonyms(word,false);
            getAntonyms(word,false);
            showUsage(word,false);
        }
        else{
            // no such case
            console.log('no such case')
        }
    })
}

const playGame = ()=>{
    // console.log('inside play')
    let status = new Spinner("Let's play a game");
    status.start();
    eventHelper.randomWord((data)=>{
        if(data.Status){
            status.stop();
            status =  new Spinner("Fetcing word details for you");
            status.start();
            let words = data.words;
            words = JSON.parse(words);
            let word = words.word;
            eventHelper.definitions(word,(definitions_data)=>{
                let definition = ""
                if(definitions_data.Status){
                    let examples = definitions_data.words;
                    words = JSON.parse(examples);
                    definition = words[0].text;
                };
                eventHelper.relatedWords(word,(related_data)=>{
                    if(related_data.Status){
                        let relatedWords = related_data.words;
                        relatedWords = JSON.parse(relatedWords);
                        let synonyms_arr = relatedWords.filter((el)=>{return el.relationshipType === 'synonym'});
                        let antonyms_arr = relatedWords.filter((el)=>{return el.relationshipType === 'antonym'});
                        let synonym_word = "";
                        let antonym_word = "";
                        if(synonyms_arr.length){
                            let synonym_words = synonyms_arr[0].words;
                            synonym_word = synonym_words[0];

                        }
                        if(antonyms_arr.length){
                            let antonym_words = antonyms_arr[0].words;
                            antonym_word = antonym_words[0];
                        }
                        status.stop()
                        if(definition){
                            console.log(chalk.cyan("A word is defined as :")+chalk.white(definition));
                        }
                        if(synonym_word){
                            console.log(chalk.cyan("A word has a synonym ")+chalk.white(synonym_word));
                        }
                        if(antonym_word){
                            console.log(chalk.cyan("A word as antonym ")+chalk.white(antonym_word));
                        }
                        const questions = [
                            {
                                name : "input_word",
                                type : "input",
                                message : "Guess and type the word.",
                                validate: function(value){
                                    if(value.length){
                                        return true;
                                    }
                                    else{
                                        return "Please guess and type the word.";
                                    }
                                }
                            }
                        ]
                        inquirer
                            .prompt(questions)
                            .then((answers)=>{
                                if(answers.input_word == word){
                                    console.log(chalk.green("Hola! Your guess is correct. ",word));
                                    return;
                                }
                                else{
                                    console.log(chalk.yellow("Sorry word is ",word));
                                    return
                                }
                            })
                    }
                    else{
                        status.stop()
                        return;
                    }
                })
            });
        }
        else{
            // console.log('done')
            return;
        }
    });
}



module.exports = {getWordDefinitions, getSynonyms, getAntonyms, showUsage, showDictionary, showDictionaryForRandomWord, playGame };